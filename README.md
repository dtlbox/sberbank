# Sberbank

Gem for payment through sberbank

## Installation

Add this line to your application's Gemfile:

    gem 'sberbank', :git => 'git@bitbucket.org:dtlbox/sberbank.git'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install sberbank

Install configuration

    $ rails g sberbank:install

And edit it

    $ vim config/initializers/sberbank_config.rb

## Usage

###Register purchasie

Start operation

    saveData = Sberbank.register(order_id,order_price,order_url)

Save returned "orderId" and redirect user to payment page
  
    sber_id = saveData["orderId"]
    redirect_to saveData["formUrl"]

### Validate purchasie

Check paid status
  
    paid = Sberbank.paid?(sber_id)

Get status of paid 

    status = Sberbank.status(sber_id)

### Cancel the payment

    Sberbank.reverse(sber_id)

### Refund money

    Sberbank.refund(sber_id, order_price)
