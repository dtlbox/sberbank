require 'rails/generators'

module Sberbank
  class InstallGenerator < Rails::Generators::Base
    desc 'Install gem "Sberbank" to your Rails application'
    source_root File.expand_path("../templates/", __FILE__)

    def create_initializer_file
      copy_file "sberbank_config.rb", Rails.root.join("config", "initializers", "sberbank_config.rb")
    end
  end
end
