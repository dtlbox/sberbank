require "sberbank/version"
require "generators/install_generator"
require "configuration/configuration"
require "openssl"
require "net/http"
require "net/https"
require "uri"
require "json"
require "addressable/uri"
module Sberbank
  REGISTER_URL = 'https://3dsec.sberbank.ru/payment/rest/register.do'
  REVERSE_URL = 'https://3dsec.sberbank.ru/payment/rest/reverse.do'
  REFUND_URL = 'https://3dsec.sberbank.ru/payment/rest/refund.do'
  STATUS_URL = 'https://3dsec.sberbank.ru/payment/rest/getOrderStatus.do'
  class <<self;  
    def register(orderId, amount, returnUrl)
      params = {
        userName: Sberbank.configuration.user_name,
        password: Sberbank.configuration.password,
        orderNumber: orderId,
        amount: amount,
        currency: Sberbank.configuration.currency,
        returnUrl: returnUrl,}
      return post(REGISTER_URL, params)      
    end

    def reverse(sberId)
      params = {
        userName: Sberbank.configuration.user_name,
        password: Sberbank.configuration.password,
        orderId: sberId,
      }
      return post(REVERSE_URL, params)
    end

    def refund(sberId, amount)
      params = {
        userName: Sberbank.configuration.user_name,
        password: Sberbank.configuration.password,
        orderId: sberId,
        amount: amount,
      }
      return post(REFUND_URL, params)
    end

    def status(sberId)
      params = {
        userName: Sberbank.configuration.user_name,
        password: Sberbank.configuration.password,
        orderId: sberId,
        language: Sberbank.configuration.language
      }
      return post(STATUS_URL, params)
    end

    def paid?(sberId)
      return status(sberId)["OrderStatus"] == 2
    end

    private

    def post(url, param)
      uri = URI.parse(url)
      https = Net::HTTP.new(uri.host, uri.port)
      https.use_ssl = true
      params = Addressable::URI.new
      params.query_values = param
      answer = https.post(uri.path, params.query).body
      JSON.parse(answer)
    end
  end
end
