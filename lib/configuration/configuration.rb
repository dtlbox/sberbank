module Sberbank
  
  class <<self; end

  def self.configuration
    @configuration ||= Configuration.new
  end

  def self.configure
    yield(configuration) if block_given?
  end

  class Configuration
    attr_accessor :user_name, :password, :currency, :language

    def initialize
    end
  end

end
